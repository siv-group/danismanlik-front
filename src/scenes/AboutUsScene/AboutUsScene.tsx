import * as React from 'react'
import i18n from '../../common/translate'
import Card from '../../components/ui/card'
import CardContent from '../../components/ui/card/card-content'
import CardTitle from '../../components/ui/card/card-title'

interface Props {
  match?: any
  history?: History
  location?: any
}

class AboutUsScene extends React.Component<Props, {}> {
  render() {
    const { props } = this
    return (
      <Card paddingRight='6em' paddingLeft='6em' paddingTop='6em' paddingBottom='6em'>
        <CardTitle>{i18n.t('menuItems.aboutUs')}</CardTitle>
        <CardContent className='padding'>
          <div dangerouslySetInnerHTML={{ __html: i18n.t('pageContents.aboutUs') }}/>
        </CardContent>
      </Card>
    )
  }
}

interface ContainerProps {
}

export default class AboutUsSceneContainer extends React.Component<ContainerProps, {}> {

  render() {
    return (
      <AboutUsScene/>
    )
  }
}
