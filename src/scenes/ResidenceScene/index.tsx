import * as React from 'react'
import i18n from '../../common/translate'
import Accordion from '../../components/functional/accordion'
import AccordionItem from '../../components/functional/accordion/accordion-item'
import AccordionItemBody from '../../components/functional/accordion/accordion-item/accordion-item-body'
import AccordionItemFooter from '../../components/functional/accordion/accordion-item/accordion-item-footer'
import AccordionItemTitle from '../../components/functional/accordion/accordion-item/accordion-item-title'
import Card from '../../components/ui/card'
import CardContent from '../../components/ui/card/card-content'
import CardTitle from '../../components/ui/card/card-title'
import { languageKeyInLocalStorage } from '../../const/local-storage'
import { FaqDTO } from '../../dtos/faq'
import { Languages } from '../../dtos/language'
import './index.scss'

interface Props {
  history?: History
  location?: any
  match?: any
  faqs: FaqDTO[]
}

class ResidenceScene extends React.Component<Props, {}> {
  render() {
    const currentLanguage = localStorage.getItem(languageKeyInLocalStorage) || Languages.RU
    const accordionItems = this.props.faqs.filter(it => it.language === currentLanguage)
                               .map((it, i) => {
                                 return (
                                   <AccordionItem key={i}>
                                     <AccordionItemTitle>
                                       <div dangerouslySetInnerHTML={{ __html: it.question }}/>
                                     </AccordionItemTitle>
                                     <AccordionItemBody>
                                       <div dangerouslySetInnerHTML={{ __html: it.answer }}/>
                                     </AccordionItemBody>
                                     <AccordionItemFooter>
                                       <button className='btn btn-primary btn-sm pull-right'>{i18n.t('call')}</button>
                                     </AccordionItemFooter>
                                   </AccordionItem>
                                 )
                               })
    return (
      <Card className='padding'>
        <CardTitle className='top-padding'>{i18n.t('questionAndAnswer')}</CardTitle>
        <CardContent className='top-padding'>
          <Accordion>
            {accordionItems}
          </Accordion>
        </CardContent>
      </Card>
    )
  }
}


interface ContainerProps {
}

export default class ResidenceSceneContainer extends React.Component<ContainerProps, {}> {
  render() {
    return (
      <ResidenceScene faqs={faq}/>
    )
  }
}

const faq = [
  {
    id: '1',
    language: 'RU',
    question: 'Вид на Жительство (İkamet Tezkeresi) в Турции',
    answer: 'Если вы приехали в Турцию и намерены остаться здесь больше безвизового режима то вам необходимо подать на Вид на Жительство (İkamet Tezkeresi). Просим вас заметить что подача на ВНЖ происходит до окончания безвизового режима, после окончания безвизового режима вы не сможете подать на ВНЖ. <br/>В таком случае вам необходимо вылететь и оплатить штраф на паспортном контроле.'
  },
  {
    id: '2',
    language: 'RU',
    question: 'Виды ВНЖ в Турции',
    answer: '-Туристический ВНЖ<br/>-Семейный ВНЖ<br/>-Студенческий ВНЖ<br/>-Длительный ВНЖ<br/>Для подробной информации просим обратить'
  },
  {
    id: '3',
    language: 'RU',
    question: 'Рабочая виза/ Разрешение на работу в Турции.',
    answer: 'Мировая глобализация, наращивание тесных экономических связей, рост конкуренции и много других объективных факторов вынуждают государства проявлять максимальную гибкость в вопросах регулирования потоков трудовой миграции. Большинство стран стремятся не только обеспечить рабочими местами собственных граждан, но и исходя из потребностей экономики привлечь дефицитных специалистов из-за рубежа. В полной мере это относится и к современной Турции.<br/>Рабочая виза в Турцию для россиян, украинцев, белорусов и других иностранцев – это одно из главных требований, выполнение которого позволит законно осуществлять профессиональную деятельность на турецкой территории. Причем норма распространяется как на обычных трудовых мигрантов, так и на зарубежных предпринимателей, планирующих открыть в Турции бизнес.'
  },
  {
    id: '4',
    language: 'RU',
    question: 'Медицинская Страховка для Иностранцев',
    answer: 'Для оформления ВНЖ в Турции вам необходимо иметь медицинскую страховку. В Основном частные медицинские страховки покрывают до 60% от общей стоимости лечения (в экстренных случаях, происшедших с вами на территории Турции). Цены на страховки зависят от вашего возраста. Для уточнения цен на Медицинскую страховку просим Вас воспользоваться ...'
  },
  {
    id: '1',
    language: 'TR',
    question: 'Türkiye’de İkamet İzni',
    answer: 'Türkiye’ye değişik nedenlerden oturmak, kısa/uzun süreli kalmak veya çalışmak için gelen yabancılar, her ne durumda olursa olsun, bir ay (30 gün) içerisinde yabancılar için düzenlenen ‘’Oturum İzni’’ veya diğer adıyla ‘’İkamet Tezkeresi’’ almak zorundadır.<br/>Oturum İzinleri belli durumlarda bulunan ya da bazı koşulları yerine getiren ve Türkiye’de oturmak kısa/uzun süreli ikamet etmek isteyen yabancılar için düzenlenir.'
  },
  {
    id: '2',
    language: 'TR',
    question: 'Türkiye’de İkamet İzni Türleri',
    answer: '-Kısa Dönem İkamet İzni (Turistik)<br/>-Aile İkamet İzni<br/>-Öğrenci İzni<br/>-Uzun Dönem İkamet İzni<br/>-Detayli bilgi almak için lütfen iletişime geçiniz'
  },
  {
    id: '3',
    language: 'TR',
    question: 'Türkiye’de Çalışma İzni',
    answer: '6735 sayılı Uluslararası İşgücü Kanunu ("Kanun"), 29800 sayılı ve 13.08.2016 tarihli Resmi Gazete\'de yayımlanmış ve yayımlandığı tarihte yürürlüğe girmiştir. Kanun’un yürürlüğe girmesiyle birlikte, 27.02.2003 tarihinde yürürlüğe giren ve uygulamada büyük önem taşıyan 4817 sayılı Yabancıların Çalışma İzinleri Hakkında Kanun yürürlükten kaldırılmıştır.<br/>Bu Kanun ile amaçlanan; uluslararası işgücüne ilişkin politikaların belirlenmesi, uygulanması, izlenmesi ile yabancılara verilecek çalışma izni ve çalışma izni muafiyetlerine dair iş ve işlemlerde izlenecek usul ve esasları, yetki ve sorumlulukları ve uluslararası işgücü alanındaki hak ve yükümlülükleri düzenlemektir.<br/>Kanun; Türkiye’de çalışmak için başvuruda bulunan veya çalışan, bir işveren yanında mesleki eğitim görmek üzere başvuruda bulunan veya bir işverenin yanında mesleki eğitim görmekte olan, staj yapmak üzere başvuruda bulunan veya staj yapan yabancılar ile Türkiye’de geçici nitelikte hizmet sunumu amacıyla bulunan sınır ötesi hizmet sunucusu yabancıları ve yabancı çalıştıran veya çalıştırmak üzere başvuruda bulunan gerçek ve tüzel kişileri kapsamaktadır. Ayrıca çalışma izni almaktan muaf tutulan bazı yabancı ülke vatandaşlarının iş ve işlemleri de bu Kanun hükümlerine göre yürütülmektedir.'
  },
  {
    id: '4',
    language: 'TR',
    question: 'Yabancılara Özel Sağlık Sigortası',
    answer: 'İkamet izni için gerekli özel sağlık sigortanızı hemen yaptırın Türkiye’de yaşamaya bir adım daha yaklaşın!<br/>Yabancıların ülkemizde ikamet edebilmesi için gerekli özel sağlık sigortası, Türkiye’deki yeni yaşamınızda sağlığını güvence altına alıyor.<br/>Yabancı Uyruklular İçin Sağlık Sigortası’nda kapsamlı teminatlar ve ayrıcalıklı hizmetler sizi bekliyor.<br/>Detayli bilgi almak için lütfen iletişime geçiniz.'
  },

  {
    id: '1',
    language: 'EN',
    question: 'Residence Permit in Turkey',
    answer: 'Turkey has started to manage the migration with the help of 6458 Law on Foreigners and International Protection with a brand new paradigm. With this paradigm, a different point of view which sees foreigners not as a burden but as diversity and promotes legal migration is adopted. In this regard, in order to conduct foreigners’ dealings and proceedings faster and more transparent new practices are put into practice.<br/>The latest reform regarding the foreigners aims to organize documents demanded for the residence permit application. With the regulation put into practice in 16 January 2015, documents demanded from foreigners during residence permit application are organized and shared with 81 provinces in order to ensure consistency.'
  },
  {
    id: '2',
    language: 'EN',
    question: 'Types of Residence Permit in Turkey',
    answer: '-Short-Term Residence Permit (Turistik)<br/>-Family Residence Permit<br/>-Student Residence Permit<br/>-Long-Term Residence Permit<br/>-For further information please contact'
  },
  {
    id: '3',
    language: 'EN',
    question: 'Work Permit in Turkey',
    answer: 'There are 4 types of foreigner work permits in Turkey. These are<br/>Work Permit for a Definite Period: In accordance with the International Labour Law No 6735 which came into force after being published on the Official Gazette dated 13.08.2016 and No 29800; work permit shall be granted to the individuals whose foreigners work permit application is approved for maximum 1 year provided that it does not exceed the term of the labour contract they signed with the employer. At the end of this period, a permit is granted for maximum 2 years to the foreigners who want to extend their work permit, provided that they continue to work in the same workplace. On the third and further application, work permit is granted up to maximum 3 years.<br/>Work Permit for an Indefinite Period: Foreigners who have long-term residence permit in Turkey and work permit for minimum 8 years are entitled to apply for a work permit for an indefinite period. Foreigners who have a work permit for an indefinite period can benefit from the rights granted to the Turkish Citizens, except for some special regulations in laws. However, they cannot benefit from rights such as military service, working at public institutions and right to vote and be elected.<br/>Independent Work Permit: Every foreigner cannot benefit from the independent work permit which is granted for a definite period. This permit is evaluated by the Ministry in accordance with the following criteria:<br/>-Level of education<br/>-Professional experience<br/>-Contribution to be made in science and technology<br/>-The contribution of the investment to be made in Turkey in the country’s economy<br/>-The capital to be invested in our country, if a shareholder of a foreign company<br/>-Turquoise Card<br/>-Turquoise Card is a card given to the foreigners who contribute in our country in the fields of economy, science, technology and industry, who invest and provide employment or who are evaluated to do so. The holders of this card benefit from all rights granted to the foreigners who have a work permit for an indefinite period. The spouses and children of holders of this card are granted a document which functions as an indefinite residence permit.<br/>First 3 years are the transition period. Within this period, if the foreigners fail to provide the expected contribution, their Turquoise Card shall be cancelled. If the holders apply for getting Turquoise Card for an indefinite period within the last 6 months of the 3-year period, Turquoise Card for an indefinite period is given if they fulfil the required criteria. However, the applications after this 3-year period expires shall not be accepted and the card they have shall expire.'
  },
  {
    id: '4',
    language: 'TR',
    question: 'Yabancılara Özel Sağlık Sigortası',
    answer: 'İkamet izni için gerekli özel sağlık sigortanızı hemen yaptırın Türkiye’de yaşamaya bir adım daha yaklaşın!<br/>Yabancıların ülkemizde ikamet edebilmesi için gerekli özel sağlık sigortası, Türkiye’deki yeni yaşamınızda sağlığını güvence altına alıyor.<br/>Yabancı Uyruklular İçin Sağlık Sigortası’nda kapsamlı teminatlar ve ayrıcalıklı hizmetler sizi bekliyor.<br/>Detayli bilgi almak için lütfen iletişime geçiniz.'
  }

]
