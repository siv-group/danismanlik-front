import {injectMethod, service} from '../../common/annotations/common'
import { db } from '../../common/firebase/firebase'
import BaseService from '../../common/services/base/base'
import {FlyTicketService} from '../index'
import {Fetcher} from '../../fetchers'
import {FlyTicketRecordStorage} from '../../storages'

@service
export default class DefaultFlyTicketService extends BaseService implements FlyTicketService {
  
  private fetcher: Fetcher
  private store: FlyTicketRecordStorage
  
  @injectMethod('Fetcher')
  setFetcher(fetch: Fetcher) {
    this.fetcher = fetch
  }
  
  @injectMethod('FlyTicketRecordStorage')
  setStore(store: FlyTicketRecordStorage){
    this.store = store
  }
  
  async getAll() {
    db.ref('/flyTickets').once('value').then((sn: any) => {
      const list = sn.val()
      const allFlyTickets = {
        list,
        totalNumber: list.length,
        loading: false
      }
      this.store.set('list', allFlyTickets)
    })
    const allFlyTicketsDefault = {
      list: [],
      totalNumber: 0,
      loading: true
    }

    this.store.set('list', allFlyTicketsDefault)
  }
  
}
