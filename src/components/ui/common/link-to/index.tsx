import * as React from 'react'
import { ReactNode } from 'react'
import { Link } from 'react-router-dom'

interface Props {
  to: string
  children?: ReactNode
  className?: string
  onClick?: () => void
}

const LinkTo = (props: Props) => {
  const { to, ...rest } = props
  const regexToExternalLink = [/^https?:\/\//, /^mailto:/]

  return (regexToExternalLink.some(it => it.test(props.to))
      ? <a target='_blank' href={to} {...rest} onClick={props.onClick}/>
      : <Link {...props} onClick={props.onClick}/>
  )
}

export default LinkTo
