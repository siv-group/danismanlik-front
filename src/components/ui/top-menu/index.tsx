import * as React from 'react'
import { slide as Menu } from 'react-burger-menu'
import i18n from '../../../common/translate'
import SelectLanguage from '../../functional/select-language'
import './index.scss'
import MenuItem from './menu-item'

interface TopMenuState {
  isOpen: boolean
}

class TopMenu extends React.Component<{}, TopMenuState> {
  state = {
    isOpen: false
  }

  onPageChange = () => {
    this.setState({ isOpen: false })
  }

  render() {
    const mainMenu = menuItems.map((it, i) =>
      (<MenuItem key={i} to={`/${it.to}`} onPageChange={this.onPageChange} className='menu-item'>{it.name}</MenuItem>))

    return (
      <Menu right burgerButtonClassName='right' isOpen={this.state.isOpen}>
        <nav className='bt-item-list'>
          {mainMenu}
          <SelectLanguage/>
        </nav>
      </Menu>
    )
  }
}

const menuItems = [
  {
    name: i18n.t('menuItems.homePage'),
    to: ''
  },
  {
    name: i18n.t('menuItems.faq'),
    to: 'faq'
  },
  {
    name: i18n.t('menuItems.medicalInsurance'),
    to: 'medical-insurance'
  },
  {
    name: i18n.t('menuItems.flights'),
    to: 'flights'
  },
  {
    name: i18n.t('menuItems.lawConsultation'),
    to: 'law-consultation'
  },
  {
    name: i18n.t('menuItems.visasToOtherCountries'),
    to: 'visas-to-other-countries'
  },
  {
    name: i18n.t('menuItems.aboutUs'),
    to: 'about-us'
  }
]

export default TopMenu

