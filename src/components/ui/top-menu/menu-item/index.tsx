import * as React from 'react'
import { ReactNode } from 'react'
import LinkTo from '../../common/link-to'
import classNames = require('classnames')

interface Props {
  to: string
  children: ReactNode
  isActive?: boolean
  className?: string
  onPageChange?: () => void
}

const MenuItem = (props: Props) => {
  const className = classNames(props.className, { active: props.isActive })
  return (<LinkTo onClick={props.onPageChange} to={props.to} className={className}>{props.children}</LinkTo>)
}

export default MenuItem
