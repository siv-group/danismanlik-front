import * as React from 'react'
import CardContent from '../../ui/card/card-content'
import LinkTo from '../../ui/common/link-to'
import './index.scss'

interface Props {
  description: string
  image: string
  linkTo: string
}

const OurService = (props: Props) => (
  <div className='our-service'>
    <LinkTo to={props.linkTo}>
      <img src={props.image} alt={props.description}/>
      <CardContent verticalPadding='0.5em' horizontalPadding='1em' className='service-desc'>
        <h5 dangerouslySetInnerHTML={{ __html: props.description }}/>
      </CardContent>
    </LinkTo>
  </div>
)

export default OurService
