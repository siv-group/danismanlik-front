import * as React from 'react'
import i18n from '../../../common/translate'
import { configuration } from '../../../configs'
import LinkTo from '../../ui/common/link-to'
import Slider from '../slick-carousel'
import './index.scss'
const mainPageSliderImage = require('../../../vendor/images/mainPageSlider.jpg')

interface Props {
}

const MainPageSlider = (props: Props) => {
  const sliderItemList = sliderItems.map((it, i) => (
    <div className='item-block' key={i}>
      <img src={it.image}/>
      <div className="carousel-caption">
        <h3>{configuration.companyName}</h3>
        <p>Foreign consulting company</p>
        <div className="top-buttons">
          <div className="bnr-button">
            <LinkTo className="act" to='about-us'>{i18n.t('aboutCompany')}</LinkTo>
          </div>
          <div className="bnr-button">
            <a href="#contacts" className="two scroll">{i18n.t('contacts')}</a>
          </div>
          <div className="clearfix"></div>
        </div>
      </div>
    </div>
  ))
  return (
    <Slider>
      {sliderItemList}
    </Slider>
  )
}

const sliderItems = [
  {
    image: mainPageSliderImage
  }
]

export default MainPageSlider
