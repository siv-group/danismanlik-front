import * as React from 'react'
import Flex from '../../ui/common/flex'
import MedicalInsuranceResult from './medical-insurance-result'
const akSigorta = require('../../../vendor/images/akSigorta.jpg')
const unicoSigorta = require('../../../vendor/images/unicoSigorta.png')
const turkNiponSigorta = require('../../../vendor/images/turkNiponSigorta.png')
const sompoJapanSigorta = require('../../../vendor/images/sompoJapanSigorta.jpg')
const groupamaSigorta = require('../../../vendor/images/groupamaSigorta.jpg')
const allianzSigorta = require('../../../vendor/images/allianzSigorta.png')

interface Props {
  gender: 'male' | 'female',
  age: number
}

const MedicalInsurancePrice = (props: Props) => {
  const priceResult = companies.map((it, i) => {
    const priceRange = it.priceList.find(it => (it.min <= props.age && it.max >= props.age))
    if (!priceRange) return
    const priceToShow = `${priceRange.price[props.gender]} TL`
    return <MedicalInsuranceResult key={i} imageUrl={it.image} companyName={it.companyName} price={priceToShow}/>
  })

  return (
    <Flex justifyContent='space-around' flexWrap='wrap'>
      {priceResult}
    </Flex>
  )
}

const companies = [
  {
    companyName: 'AK Sigorta',
    image: akSigorta,
    priceList: [
      {min: 0, max: 18, price: {male: 161, female: 161}},
      {min: 19, max: 25, price: {male: 145, female: 145}},
      {min: 26, max: 30, price: {male: 195, female: 195}},
      {min: 31, max: 35, price: {male: 209, female: 209}},
      {min: 36, max: 40, price: {male: 235, female: 235}},
      {min: 41, max: 45, price: {male: 309, female: 309}},
      {min: 46, max: 50, price: {male: 310, female: 310}},
      {min: 51, max: 55, price: {male: 410, female: 410}},
      {min: 56, max: 60, price: {male: 460, female: 460}},
      {min: 61, max: 64, price: {male: 740, female: 740}},
      {min: 65, max: 70, price: {male: 1100, female: 1100}}
    ]
  },
  {
    companyName: 'Unico Sigorta',
    image: unicoSigorta,
    priceList: [
      {min: 0, max: 18, price: {male: 165, female: 165}},
      {min: 19, max: 25, price: {male: 148, female: 148}},
      {min: 26, max: 30, price: {male: 195.2, female: 195.2}},
      {min: 31, max: 35, price: {male: 204, female: 204}},
      {min: 36, max: 40, price: {male: 232, female: 232}},
      {min: 41, max: 45, price: {male: 300, female: 300}},
      {min: 46, max: 50, price: {male: 300, female: 300}},
      {min: 51, max: 55, price: {male: 400, female: 400}},
      {min: 56, max: 60, price: {male: 464, female: 464}},
      {min: 61, max: 65, price: {male: 740, female: 740}}
    ]
  },
  {
    companyName: 'Turk Nippon Sigorta',
    image: turkNiponSigorta,
    priceList: [
      {min: 0, max: 18, price: {male: 147.25, female: 147.25}},
      {min: 19, max: 25, price: {male: 150.35, female: 150.35}},
      {min: 26, max: 30, price: {male: 190.66, female: 190.66}},
      {min: 31, max: 35, price: {male: 204.6, female: 204.6}},
      {min: 36, max: 40, price: {male: 210.8, female: 210.8}},
      {min: 41, max: 50, price: {male: 275.9, female: 275.9}},
      {min: 51, max: 60, price: {male: 393.7, female: 393.7}},
      {min: 61, max: 70, price: {male: 697.5, female: 697.5}}
    ]
  },
  {
    companyName: 'Sompo Japan Sigorta',
    image: sompoJapanSigorta,
    priceList: [
      {min: 0, max: 18, price: {male: 187, female: 187}},
      {min: 19, max: 25, price: {male: 145, female: 145}},
      {min: 26, max: 30, price: {male: 180, female: 180}},
      {min: 31, max: 35, price: {male: 180, female: 180}},
      {min: 36, max: 40, price: {male: 206, female: 206}},
      {min: 41, max: 45, price: {male: 268, female: 268}},
      {min: 46, max: 50, price: {male: 275, female: 275}},
      {min: 51, max: 55, price: {male: 372, female: 372}},
      {min: 56, max: 60, price: {male: 438, female: 438}},
      {min: 61, max: 65, price: {male: 1004, female: 1004}}
    ]
  },
  {
    companyName: 'Groupama Sigorta',
    image: groupamaSigorta,
    priceList: [
      {min: 0, max: 18, price: {male: 200, female: 200}},
      {min: 19, max: 25, price: {male: 150, female: 150}},
      {min: 26, max: 30, price: {male: 200, female: 200}},
      {min: 31, max: 35, price: {male: 215, female: 215}},
      {min: 36, max: 40, price: {male: 240, female: 240}},
      {min: 41, max: 45, price: {male: 320, female: 320}},
      {min: 46, max: 50, price: {male: 320, female: 320}},
      {min: 51, max: 55, price: {male: 420, female: 420}},
      {min: 56, max: 60, price: {male: 480, female: 480}},
      {min: 61, max: 64, price: {male: 750, female: 750}}
    ]
  },
  {
    companyName: 'Allianz Sigorta',
    image: allianzSigorta,
    priceList: [
      {min: 0, max: 18, price: {male: 285.44, female: 285.44}},
      {min: 19, max: 25, price: {male: 175.93, female: 175.93}},
      {min: 26, max: 30, price: {male: 232.96, female: 232.96}},
      {min: 31, max: 35, price: {male: 242.03, female: 242.03}},
      {min: 36, max: 40, price: {male: 275.40, female: 275.40}},
      {min: 41, max: 49, price: {male: 363.20, female: 363.20}},
      {min: 50, max: 55, price: {male: 799.63, female: 799.63}},
      {min: 56, max: 60, price: {male: 856.66, female: 856.66}},
      {min: 61, max: 64, price: {male: 1142.10, female: 1142.10}}
    ]
  }
]

export default MedicalInsurancePrice
