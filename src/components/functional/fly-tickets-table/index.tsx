import { observer } from 'mobx-react'
import * as React from 'react'
import Moment from 'react-moment'
import { instanceRegistry } from '../../../common/annotations/common'
import i18n from '../../../common/translate'
import { configuration } from '../../../configs/index'
import { FlyTicketMapper } from '../../../mappers'
import FlyTicket from '../../../models/fly-ticket'
import { FlyTicketService } from '../../../services'
import LinkTo from '../../ui/common/link-to'
import Table from '../table'
import './index.scss'

interface Props {
  history?: History
  location?: any
  match?: any
  loading: boolean
  flyTickets: FlyTicket.Model[]
}

class FlyTicketsTable extends React.Component<Props, {}> {
  render() {


    return (
      <div className='fly-tickets-table'>
        <Table data={this.props.flyTickets} columns={tableColumns} loading={this.props.loading}/>
      </div>
    )
  }
}

interface ContainerProps {
  lightVersion?: boolean
}

@observer
export default class FlyTicketsTableContainer extends React.Component<ContainerProps, {}> {
  private flyTicketService: FlyTicketService = instanceRegistry.get('FlyTicketService')
  private flyTicketMapper: FlyTicketMapper = instanceRegistry.get('FlyTicketMapper')

  componentWillMount() {
    this.flyTicketService.getAll()
  }

  render() {
    const allFlyTickets = this.flyTicketMapper.all.list.map(it => ({
      id: it.id,
      from: it.from,
      to: it.to,
      way: it.way,
      departureTime: it.departureTime,
      arrivalTime: it.arrivalTime,
      airline: it.airline,
      price: it.price,
      remainingPlaces: it.remainingPlaces
    }))

    return (
      <div>
        <FlyTicketsTable flyTickets={allFlyTickets} loading={this.flyTicketMapper.all.loading}/>
      </div>
    )
  }
}


const tableColumns = [
  {
    Header: i18n.t('whereFrom'),
    accessor: 'from'
  },
  {
    Header: i18n.t('to'),
    accessor: 'to'
  },
  {
    Header: i18n.t('way'),
    accessor: 'way'
  },
  {
    Header: i18n.t('departureTime'),
    id: 'departureTime',
    accessor: 'departureTime'
  },
  {
    Header: i18n.t('arrivalTime'),
    id: 'arrivalTime',
    accessor: 'arrivalTime'
  },
  {
    Header: i18n.t('airline'),
    accessor: 'airline'
  },
  {
    Header: i18n.t('price'),
    id: 'price',
    accessor: (d: FlyTicket.Model) => `${d.price.value} ${d.price.currency}`
  },
  {
    Header: i18n.t('buy'),
    id: 'remainingPlaces',
    accessor: () => <LinkTo to={`mailto:${configuration.contacts.emailAddress}`}>{i18n.t('specify')}</LinkTo>
  }
]
