import * as React from 'react'
import { languageKeyInLocalStorage } from '../../../const/local-storage'
import { Languages } from '../../../dtos/language'

const SelectLanguage = () => {
  const availableLanguages = [Languages.RU, Languages.EN, Languages.TR]
  const selectedLanguage = localStorage.getItem(languageKeyInLocalStorage) || Languages.RU
  const handleChangeLanguage = (e: any) => {
    const newSelectedLanguage = e.target.value
    localStorage.setItem(languageKeyInLocalStorage, newSelectedLanguage)
    location.reload()
  }
  const optionList = availableLanguages.map((it, i) => (
    <option value={it} key={i}>{it}</option>)
  )
  return (
    <select onChange={handleChangeLanguage} defaultValue={selectedLanguage}>
      {optionList}
    </select>
  )
}

export default SelectLanguage
