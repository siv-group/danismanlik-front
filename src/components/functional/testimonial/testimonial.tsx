import * as React from 'react'
import './index.scss'
import i18n from '../../../common/translate'
import { SupportedLanguages } from '../../../const/language'
import { languageKeyInLocalStorage } from '../../../const/local-storage'
import Slider from '../slick-carousel'
import TestimonialItem from './testimonail-item'
const elenaSkrebatun = require('../../../vendor/uploads/elenaSkrebatun.png')
const dilaUzakova = require('../../../vendor/uploads/dilaUzakova.jpg')
const innaYildirim = require('../../../vendor/uploads/İnnaYildirim.jpg')

interface Props {
  history?: History
  location?: any
  match?: any
  testimonials: any[]
}

class Testimonial extends React.Component<Props, {}> {
  render() {
    const testimonialItems = this.props.testimonials.map((it, i) => (
      <TestimonialItem {...it} key={i}/>
    ))

    return (
      <div>
        <div className="testimonials">
          <div className="container">
            <h3 className="tittle-w3ls cen">{i18n.t('feedback')}</h3>
            <Slider showArrows={false} autoPlay={true}>
              {testimonialItems}
            </Slider>
          </div>
        </div>
      </div>
    )
  }
}

interface ContainerProps {
}

export default class TestimonialContainer extends React.Component<ContainerProps, {}> {
  render() {
    const currentLanguage = localStorage.getItem(languageKeyInLocalStorage)
    const testimonialsByLanguage = testimonials.filter(it => it.language === currentLanguage)
    return (
      <div>
        <Testimonial testimonials={testimonialsByLanguage}/>
      </div>

    )
  }
}

const testimonials = [
  {
    image: elenaSkrebatun,
    fullName: 'Елена Скребатун',
    message: 'Очень хорошо знают свою работу, приятно иметь дело. Желаю вам больших успехов во всех начинаниях!!! Спасибо вам за помощь.',
    language: SupportedLanguages.ru
  },
  {
    image: dilaUzakova,
    fullName: 'Диля Узакова',
    message: 'Быстро, качественно и доступно. Самое главное, что не за кем бегать не нужно. Сами звонят и напоминают о дальнейших действиях. Работу выполняют честно и консультируют очень доступно для мозгов.',
    language: SupportedLanguages.ru
  },
  {
    image: innaYildirim,
    fullName: 'Инна Йылдырым',
    message: 'Спасибо вам большое за помощь. Делала сыну туристическую визу и понятия не имела с чего начать. Благодаря вам у нас все получилось и сегодня мы получили на руки карту. Спасибо вам большое.',
    language: SupportedLanguages.ru
  },
  {
    fullName: 'Bülent YILMAZ',
    message: 'Rusya vize için bana yardım oldular. Hizmet baya güzeldi, her şey zamanında. Teşekkür ederim.',
    language: SupportedLanguages.tr
  },
  {
    fullName: 'Alex Ten',
    message: 'They help in all matters, workers do the work honestly. Thank you.',
    language: SupportedLanguages.en
  },
]
