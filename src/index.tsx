import * as mobx from 'mobx'
import * as React from 'react'
import * as ReactDOM from 'react-dom'

import App from './App'
import { disposeInjection } from './common/annotations/common'
import { classLoader } from './common/class-loader'
import { translationReady } from './common/translate'
import registerServiceWorker from './registerServiceWorker'

mobx.useStrict(true)

classLoader()
  .then(() => disposeInjection())
  .then(() => translationReady)
  .then(() => ReactDOM.render(
    <App/>,
    document.getElementById('root')
  ))

registerServiceWorker()
