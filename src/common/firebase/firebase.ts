import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

const config = {
  apiKey: 'AIzaSyDr1LopwPXlOjSUtkBQ-Z6y65nVwjNY_Us',
  authDomain: 'danishmanlik-35eb7.firebaseapp.com',
  databaseURL: 'https://danishmanlik-35eb7.firebaseio.com',
  projectId: 'danishmanlik-35eb7',
  storageBucket: 'danishmanlik-35eb7.appspot.com',
  messagingSenderId: '896551641562',
  // appId: '1:896551641562:web:1154bf4e1e51036da6e0e3'
}

const { default: defaultFirebase } = firebase as any
if (!defaultFirebase.apps.length) {
  defaultFirebase.initializeApp(config)
}

export const auth = defaultFirebase.auth()
export const db = defaultFirebase.database()
