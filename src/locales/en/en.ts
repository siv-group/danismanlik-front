import { configuration } from '../../configs/index'

const menuItems = {
  'homePage': 'Home',
  'faq': 'FAQ',
  'medicalInsurance': 'Medical insurance',
  'flights': 'Flights',
  'lawConsultation': 'Law consultation',
  'visasToOtherCountries': 'Visas to other countries',
  'aboutUs': 'About us'
}

const serviceItems = {
  'residencePermit': 'Residence Permit,<br/> Working visa to Turkey,<br/> Visa to other countries',
  'lawConsultation': 'Law Consultation,<br/> Document Translation',
  'medicalInsurance': 'Medical insurance',
  'workInTurkey': 'Work in Turkey'
}

const pageContents = {
  lawConsultationPageContent: 'Along with investments and foreign business appearance in Turkey, the legal services of different kinds have become as popular as never before.<br/>Legal questions to be settled arise in the process of any kind of business. Contract legal services offer the possibility to settle these problems with the help of legal services on a scheduled basis.<br/>Our team of lawyers has a pool of experience in providing such services to the big companies that have their own legal department as well as to the small private firms that don’t have their own lawyer in staff. Complex legal business support has a range of advantages: cost reduction of taxes, increase of budget effectiveness and predictability.',
  visasToOtherCountries: 'Our Company is opened in order to improve the service quality for Turkey citizens, citizens of other countries and persons without citizenship wishing to obtain a visa to the other countries. S&B Consulting will give you support, will provide comfort and convenience during the other countries visa processing.',
  aboutUs: `<b>${configuration.companyName}</b> team was created in 2017. Throughout the time, our team worked hard for you - our friends!<br/><br/>The staff of the company employs only true professionals, who are always ready to answer your questions. All employees of the company speak Russian, Turkish and English languages.<br/><br/>What is the difference?<br/><br/>It's very simple, each of us passed this way. Each of us came to this beautiful country, young and naive. Each of us did not always get honest people. And in the end, having passed this not easy way, we decided to open a company that will honestly fight for you - our dear friends!<br/><br/>With respect, <b>${configuration.companyName}</b>`
}

const enTranslation = {
  'whereFrom': 'From',
  'to': 'Where',
  'way': 'Way',
  'departureTime': 'Departure Time',
  'arrivalTime': 'Arrival Time',
  'airline': 'Airline',
  'price': 'Price',
  'buy': 'Buy',
  'specify': 'Specify',
  'services': 'Our Services',
  'aboutCompany': 'About Company',
  'contacts': 'Contacts',

  'followUs': 'Follow us on social networks',
  'allRightReserved': 'All Right Reserved',
  'ourPhones': 'Our Phones',
  'writeToUs': 'Write to us',
  'ourAddress': 'Our Address',
  'call': 'Call',
  'questionAndAnswer': 'Questions and Answers',
  'male': 'Male',
  'female': 'Female',
  'age': 'Age',
  'gender': 'Gender',
  'enterYourGenderAndAge': 'Enter your Gender and Age',
  'findOutTheCostOfHealthInsurance': 'Find out The Cost of Health Insurance',
  'notFound': 'Not Found',
  'feedback': 'Feedback',

  menuItems,
  serviceItems,
  pageContents
}

export default enTranslation
