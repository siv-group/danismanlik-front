import { configuration } from '../../configs'

const menuItems = {
  'homePage': 'Ana Sayfa',
  'faq': 'FAQ',
  'medicalInsurance': 'Sağlık Sigortası',
  'flights': 'Uçuşlar',
  'lawConsultation': 'Hukuk Danışmanlığı',
  'visasToOtherCountries': 'Diğer Ülkelere Vize',
  'aboutUs': 'Hakkımızda'
}

const serviceItems = {
  'residencePermit': 'İkamet İzni,<br/> Çalışma İzni,<br/> Diğer Ülkelere Vize',
  'lawConsultation': 'Hukuk Danışmanlığı,<br/> Tercüme',
  'medicalInsurance': 'Sağlık Sigortası',
  'workInTurkey': 'Türkiye’de İş Bulma'
}

const pageContents = {
  lawConsultationPageContent: 'Yabancılar ve Vatandaşlık Hukuku, Türkiye’deki yabancıların kişisel ve maddi haklarını kapsayan hukuk dalıdır. İkamet ve seyahatleri, çalışma izinlerini, evlilik ve boşanma işlemlerini, T.C vatandaşından çocuk sahibi olma ve nüfus kayıt işlemleri ile vatandaşlık başvuru işlemlerini içerir.<br/>Kolay olmayan prosedürler çerçevesinde S&B İnsan Kaynakları ve Danışmanlık yabancılar ve vatandaşlık hukuku alanlarında profesyonel dava ve danışmanlık hizmeti sunmakta, özellikle yabancıların Türkiye’ye gelirken ve Türkiye’de yaşadıkları hukuki problemlerin çözümünü sağlamaktadır.',
  visasToOtherCountries: 'Şirketimiz Diğer ülkelere vize almak isteyen Türkiye Cumhuriyeti vatandaşlarına, diğer ülke vatandaşları ve vatandaşlığı olmayan kişilere sunulacak hizmet kalitesinin yükseltilmesi amacı ile açılmıştır. S&B İnsan Kaynakları ve Danışmanlık size destek verecek, aynı zamanda Diğer Ülkelere vizelerinin düzenlenmesinde konfor ve rahatlık sağlayacaktır.',
  aboutUs: `<b>${configuration.companyName}</b> olarak, Personel İstihdamı, Çalışma izni, Yabancı sermayeli şirket kuruluşu, İkamet izni (oturma izni ), Türk İş Hukuku ve Türk Vatandaşlığı gibi başlıca konularda tam profesyonel yetkinlik ile hizmet verebilen Danışmanlık firmasıyız.<br/><br/>Müşterilerimizin istek ve beklentileri üzerine yoğunlaşan hizmetlerimiz ve çözümler üretmedeki becerilerimiz ile etik ve ahlaki kurallar çerçevesinde hizmet veren firmamız, sağladığı desteği sadece yasalar ve yayınlanmış yönetmelikler çerçevesinde sunmaktadır.<br/><br/>Saygılarımızla, <b>${configuration.companyName}</b>`
}

const trTranslation = {
  'whereFrom': 'Nereden',
  'to': 'Nereye',
  'way': 'Yol',
  'departureTime': 'Kalkış Saati',
  'arrivalTime': 'İniş Saati',
  'airline': 'Havayolu',
  'price': 'Fiyat',
  'buy': 'Satın Almak',
  'specify': 'Netleştirin',
  'services': 'Hizmetlerimiz',
  'aboutCompany': 'Şirket Hakkında',
  'contacts': 'İletişim Bilgileri',

  'followUs': 'Bizi Sosyal Ağlarda Takip Edin',
  'allRightReserved': 'Tüm Hakları Saklıdır',
  'ourPhones': 'Telefon',
  'writeToUs': 'Bize Yazın',
  'ourAddress': 'Adres',
  'call': 'Arayın',
  'questionAndAnswer': 'Sorular/Cevaplar',
  'male': 'Bay',
  'female': 'Bayan',
  'age': 'Yaş',
  'gender': 'Cinsiyet',
  'enterYourGenderAndAge': 'Cinsiyetinizi ve Yaşınızı Giriniz',
  'findOutTheCostOfHealthInsurance': 'Sağlık sigortasının fiyatını öğrenin',
  'notFound': 'Bulunamadı',
  'feedback': 'Geri bildirim',

  menuItems,
  serviceItems,
  pageContents
}

export default trTranslation
